/**
 *
 *  Ternary Operator
 * */

public class TernaryOperator_10 {

    public static void main(String[] args) {

        int a = 2;
        int b = 3;

        int min = (a < b)? a : b;

        System.out.println(min);
    }
}
